$(document).ready(function () {

  $('#video').YTPlayer({
    fitToBackground: true,
    videoId: 'XNfrsopih0w',
    playerVars: {
      modestbranding: 0,
      autoplay: 1,
      controls: 0,
      showinfo: 0,
      branding: 0,
      rel: 0,
      autohide: 0
    }
  });

  $(window).resize(function () {

    if ($(window).width() < 1024) {
      $('.sw-top').addClass('sw-top_static');
    } else {
      $('.sw-top').removeClass('sw-top_static');
    }

  });

  $('#form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var email = $('input[name="email"]', f).val();
    var error = false;
    if (person == '') {
      $('input[name="person"]', f).addClass('ierror');
      error = true;
    }
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (email == '') {
      $('input[name="email"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    /*
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        window.location.href = 'https://docs.google.com/spreadsheets/d/1lurC5pLgaGm9klNJ_UcLJqdXl11dBpLs-GD365TszLc/edit?usp=sharing';
      }
      else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });*/
    return false;
  });

});
